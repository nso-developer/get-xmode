# get-xmode

An NSO package to get the mode (read-only or read-write) of a transaction.

This should really be a builtin function in NSO, but until that's available,
this is what we got to work with.

## Calling from Python

```python
def find_transaction_mode(th, maapi):
    gx = ncs.maagic.get_node(maapi, '/gx:get-xmode/get-xmode')
    gxi = gx.get_input()
    gxi.tid = th
    return gx(gxi).xmode
```

## An actually very nice, cool, elegant, awesome and beautiful action wrapper
This is a wrapper for wrapping around an action that performs configuration
changes. It's very cool.

- attach to existing configuration transaction or open new
- automatic dry-run handling
- automatic error handling

If you are in operational mode, it will open a new transaction, write the
changes in there and commit the transaction at the end. However, if you are
already in a configuration transaction when you call the action, the action
will attach to your currently open transaction, write the changes in there and
NOT commit. Since you already have a configuration transaction open, the action
leaves it up to you to commit the transaction, or not. This makes it possible
for you to use the NSO CLI (or web UI) to inspect the candidate configuration
that's produced by the action, you can also modify that configuration before
committing it. The wrapper also honours dry-run, so that if it is specified,
the wrapper takes care of honouring dry-run, that is, it will not apply any
changes. You are expected to add a dry-run leaf to your action inputs.

Any exceptions that are raised in your action python function (and not caught
within the function) will be handled by the wrapper, which treats them as
errors and aborts the transaction if it started a new one or just detaches from
the existing one. It will include the exception message in the action output.

```python
import functools

def ActionAutoTrans(context):
    def decorator(fn):
        @functools.wraps(fn)
        def wrapper(self, uinfo, name, kp, action_input, action_output, t_write):
            with ncs.maapi.Maapi() as m:
                with ncs.maapi.Session(m, uinfo.username, context):
                    xmode = find_transaction_mode(uinfo.actx_thandle, m)
                    try:
                        if xmode in ('read_write', 'read_write_operational'):
                            t_write = m.attach(uinfo.actx_thandle)
                        else:
                            t_write = m.start_write_trans()

                        fn(self, uinfo, name, kp, action_input, action_output, t_write)

                        # Apply changes if not in dry-run mode
                        if not action_input.dry_run and xmode != 'read_write':
                            t_write.apply()

                    except Exception as e:
                        self.log.error(f"{name} {kp}: action error {e}")
                        self.log.error(traceback.format_exc())
                        action_output.success = False
                        action_output.message = str(e)
                    finally:
                        if t_write and xmode != 'read_write':
                            t_write.finish()
                        if xmode in ('read_write', 'read_write_operational'):
                            m.detach(uinfo.actx_thandle)
        return wrapper
    return

def find_transaction_mode(th, maapi):
    gx = ncs.maagic.get_node(maapi, '/gx:get-xmode/get-xmode')
    gxi = gx.get_input()
    gxi.tid = th
    return gx(gxi).xmode


class ExampleAction(Action):
    @Action.action
    @ActionAutoTrans('python-example-action')
    def cb_action(self, uinfo, name, kp, action_input, action_output, t_write):
        self.log.info('Running example action', kp)
        # do stuff

```

```yang
container foo {
  action example-action {
    tailf:actionpoint "example-action";
    input {
      leaf dry-run {
        type boolean;
      }
    }
    output {
      leaf success {
        type boolean;
      }
      leaf message {
        type string;
      }
    }
  }
}
```
